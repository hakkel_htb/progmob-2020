package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Crud.DosenUpdateActivity;
import ukdw.com.progmob_2020.Crud.MatakuliahUpdateActivity;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.MataKuliah;
import ukdw.com.progmob_2020.R;

public class MatakuliahCRUDRecyclerAdapter extends RecyclerView.Adapter<MatakuliahCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<MataKuliah> matakuliahList;


    public MatakuliahCRUDRecyclerAdapter(Context context) {
        this.context = context;
        matakuliahList = new ArrayList<>();
    }

    public MatakuliahCRUDRecyclerAdapter(List<MataKuliah> matakuliahList) {
        this.matakuliahList = matakuliahList;
    }


    public List<MataKuliah> getMatakuliahList() {
        return matakuliahList;
    }

    public void setMatakuliahList(List<MataKuliah> matakuliahList) {
        this.matakuliahList = matakuliahList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MataKuliah mk = matakuliahList.get(position);

        holder.tvnama.setText(mk.getNama());
        holder.tvkodemk.setText(mk.getKode());
        holder.tvharimk.setText(mk.getHari());
        holder.tvsesimk.setText(mk.getSesi());
        holder.tvsksmk.setText(mk.getSks());
        holder.mk = mk;
    }


    @Override
    public int getItemCount() {
        return matakuliahList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvnama,tvkodemk,tvharimk,tvsesimk,tvsksmk;
        private RecyclerView rvMatakuliah;
        MataKuliah mk;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvnama = itemView.findViewById(R.id.tvnamamk);
            tvkodemk = itemView.findViewById(R.id.tvkodemk);
            tvharimk = itemView.findViewById(R.id.tvharimk);
            tvsesimk = itemView.findViewById(R.id.tvsesimk);
            tvsksmk = itemView.findViewById(R.id.tvsksmk);
            rvMatakuliah = itemView.findViewById(R.id.rvMatakuliah);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), MatakuliahUpdateActivity.class);
                    intent.putExtra("nama",mk.getNama());
                    intent.putExtra("kode",mk.getKode());
                    intent.putExtra("hari",mk.getHari());
                    intent.putExtra("sesi",mk.getSesi());
                    intent.putExtra("sks",mk.getSks());

                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}