package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.progmob_2020.Adapter.MatakuliahCRUDRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Model.MataKuliah;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MataKuliahGetAllActivity extends AppCompatActivity {

    RecyclerView rvMtkl;
    MatakuliahCRUDRecyclerAdapter mtklAdapter;
    ProgressDialog pd;
    List<MataKuliah> matakuliahList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mata_kuliah_get_all);

        rvMtkl = (RecyclerView)findViewById(R.id.rvMatakuliah);
        pd = new ProgressDialog(this);
        pd.setTitle("Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MataKuliah>> call = service.getMatkul("72180256");

        call.enqueue(new Callback<List<MataKuliah>>() {
            @Override
            public void onResponse(Call<List<MataKuliah>> call, Response<List<MataKuliah>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                mtklAdapter = new MatakuliahCRUDRecyclerAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MataKuliahGetAllActivity.this);
                rvMtkl.setLayoutManager(layoutManager);
                rvMtkl.setAdapter(mtklAdapter);

            }

            @Override
            public void onFailure(Call<List<MataKuliah>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MataKuliahGetAllActivity.this,"error", Toast.LENGTH_LONG);
            }
        });
    }
}