package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import ukdw.com.progmob_2020.Login.LoginActivity;
import ukdw.com.progmob_2020.R;

public class MainMenuActivity extends AppCompatActivity {
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ImageView btnDataMhs = (ImageView)findViewById(R.id.imageViewMhs);
        ImageView btnDataDsn = (ImageView)findViewById(R.id.imageViewDsn);
        ImageView btnDataMatkul = (ImageView)findViewById(R.id.imageViewMatkul);
        ImageView btnDataLogout = (ImageView)findViewById(R.id.imageViewLogout);

        Toast.makeText(MainMenuActivity.this, "Silahkan Dipilih Ya", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(MainMenuActivity.this);

        if(session.getString("nimnik", "").isEmpty() && session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(MainMenuActivity.this, LoginActivity.class));
            return;
        }

        btnDataMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this,MainMhsActivity.class);
                startActivity(intent);
            }
        });

        btnDataDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this,DosenActivity.class);
                startActivity(intent);
            }
        });

        btnDataMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenuActivity.this,MataKuliahActivity.class);
                startActivity(intent);
            }
        });

        btnDataLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = session.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent intent = new Intent(MainMenuActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}