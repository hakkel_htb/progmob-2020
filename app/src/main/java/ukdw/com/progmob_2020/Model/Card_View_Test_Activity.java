package ukdw.com.progmob_2020.Model;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Adapter.MahasiswaCardAdapter;
import ukdw.com.progmob_2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob_2020.Pertemuan2.RecyclerActivity;
import ukdw.com.progmob_2020.R;

public class Card_View_Test_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card__view__test_);

        RecyclerView cv = (RecyclerView)findViewById(R.id.cvSoal);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        //Data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //Generate Data Mahasiswa // 72180256
        Mahasiswa m1 = new Mahasiswa("Hakkel Hutabarat", "72180256","081216046");
        Mahasiswa m2 = new Mahasiswa("Ucok Parbada", "72180198","08122522");
        Mahasiswa m3 = new Mahasiswa("Laeku Naburju", "72180299","0812149854");
        Mahasiswa m4 = new Mahasiswa("Ruendi Simajuntak", "72180255","081212326");
        Mahasiswa m5 = new Mahasiswa("Alfina Desta", "72180269","089996046");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(Card_View_Test_Activity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        cv.setLayoutManager(new LinearLayoutManager(Card_View_Test_Activity.this));
        cv.setAdapter(mahasiswaCardAdapter);
    }

}