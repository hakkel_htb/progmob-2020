package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class MataKuliahActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mata_kuliah);
        Button btnMtkl = (Button)findViewById(R.id.buttonGetMtkl);
        Button btntambahMtkl = (Button)findViewById(R.id.buttonnAddMtkl);
        Button btnupdtMtkl = (Button)findViewById(R.id.btnupdatedatamatkul);
        Button btndelMtkl = (Button)findViewById(R.id.buttonDeleteMtkl);

        btnMtkl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MataKuliahActivity.this, MataKuliahGetAllActivity.class);
                startActivity(intent);
            }
        });

        btntambahMtkl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MataKuliahActivity.this, MatakuliahAddActivity.class);
                startActivity(intent);
            }
        });

//        btnupdtMtkl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MataKuliahActivity.this, MatakuliahUpdateActivity.class);
//                startActivity(intent);
//            }
//        });

        btndelMtkl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MataKuliahActivity.this, MatakuliahHapusActivity.class);
                startActivity(intent);
            }
        });
    }
}