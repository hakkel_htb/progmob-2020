package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class DosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen);
        Button btnliat = (Button)findViewById(R.id.buttonGetDsn);
        Button btntambah = (Button)findViewById(R.id.buttonnAddDsn);
        Button btnupdt = (Button)findViewById(R.id.buttonUpdateDsn);
        Button btndel = (Button)findViewById(R.id.buttonDeleteDsn);

        btnliat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });

        btntambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });

      btnupdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });

        btndel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, DosenHapusActivity.class);
               startActivity(intent);
          }
        });
    }
}